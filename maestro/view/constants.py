#!/usr/bin/env python3

# This file is part of maestro, a keyboard-driven configurable music player.
# Copyright (C) 2022  Baptiste Lambert (Blaireau) rabbitstemplate@disroot.org
#
# Maestro is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

APP_TITLE = "Maestro"
APP_ICON = "icon/maestro.png"
MAESTRO_MOTTO = "A configurable and keyboard-centric music player"

# height in pixel of each album item
ALBUM_ITEM_HEIGHT = 200
# width in pixel of each album item
ALBUM_ITEM_WIDTH = 180

# height in pixel of each song item
SONG_ITEM_HEIGHT = 50

# scroll
SCROLL_STEP = 100

# delays
PING_REFRESH_RATE = 0.1
SONG_TRANSITION_DELAY = 1
RESEND_KEY_DELAY = 0.1

